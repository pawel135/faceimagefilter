#!/bin/bash
SRC=$HOME/Downloads/Faces/FaceScrub/facescrub_aligned
DEST=$HOME/Downloads/Faces/FaceScrub/filtered
DEST_DIR_BEGINNING='facescrub_top'
HOW_MANY_TOP_DIRS=(10 20 30 40)
#HOW_MANY_TOP_DIRS=(10 20 30 40)
WHAT_TO_REMOVE_IN_DEST_DIRS='*json*'

IFS=$'\n' HOW_MANY_SORTED=($(sort <<<"${HOW_MANY_TOP_DIRS[*]}"))
unset IFS

echo "Create empty directories"
for s in ${HOW_MANY_SORTED[@]}; do
    DEST_DIR=$DEST/$DEST_DIR_BEGINNING"_"$s
    printf "$(tput setaf 2) Will create directory ${DEST_DIR}$(tput sgr 0)\n"
    mkdir -p $DEST_DIR
done

SORTED_DIRS=( $(du -hs $SRC/* | sort -hr | awk '{print $2}') )
printf "$(tput setaf 3) All directories in the order from biggest to smallest:$(tput sgr 0)\n"
echo ${SORTED_DIRS[@]}
for i in ${!HOW_MANY_SORTED[@]}; do
    q=${HOW_MANY_SORTED[$i]} #quantity
    lq=0 && [ $i -gt 0 ] && lq=${HOW_MANY_SORTED[$((i-1))]} #last quantity
    printf "\nSelecting #$i element that requires top $q directories\n"
    DIFF=$((q-lq))
    printf "Will take only next $DIFF as previous files were already copied\n"
    SRC_DIRS=("${SORTED_DIRS[@]:lq:DIFF}")
    SRC_DIRS_PATH=( "${SRC_DIRS[@]/#/$SRC/}" )
    echo ${SRC_DIRS[@]}
    SORTED_DEST_DIRS=${HOW_MANY_SORTED[@]:i}
    for sdd in ${SORTED_DEST_DIRS[@]}; do
        DEST_DIR=$DEST/$DEST_DIR_BEGINNING"_"$sdd
        printf "$(tput setaf 2)Moving above directories to ${DEST_DIR} $(tput sgr 0)\n"
        echo ${DEST_DIR}
        #cp -r --parents `find $SRC_DIRS -name ${WHAT_TO_COPY_IN_DIRS}` ${DEST_DIR}/
        #cp -r `find $SRC_DIRS_PATH -name ${WHAT_TO_COPY_IN_DIRS}` ${DEST_DIR}
        cp -r ${SRC_DIRS[@]} ${DEST_DIR}
        #cp -r $SRC_DIRS/${WHAT_TO_COPY_IN_DIRS} ${DEST_DIR}
        printf "Removing dest files matching regex: ${WHAT_TO_REMOVE_IN_DEST_DIRS}\n"
        rm `find ${DEST_DIR}  -name ${WHAT_TO_REMOVE_IN_DEST_DIRS}`
        
    done
done


#du -hs * | sort -hr | awk '{print $2'} | head -20 | tail -10 | xargs echo

